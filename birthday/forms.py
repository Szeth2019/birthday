from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
# make my own version of below later
from birthday.models import Comment, Member, Post

class LeaveMessage(FlaskForm):
    """Submit a note to Kinsey"""
    name = StringField('Name', validators = [DataRequired()])
    note = TextField('Note', validators = [DataRequired(), Length(min=4, message=('Your message is too short.'))])
    submit = SubmitField('Submit')

class CreateUser(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    bio = TextField('Bio', validators = [DataRequired(), Length(min=4, message=('Your message is too short.'))])
    submit = SubmitField('Sign Up')

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')


    #When you add any methods that match the pattern validate_<field_name>, WTForms takes those 
    # as custom validators and invokes them in addition to the stock validators
    def validate_username(self, username):
        user = Member.query.filter_by(username=username.data).first()
        # raise error
        # inform Flask-WTF that the field is invalid
        if user is not None:
            raise ValidationError('Please use a different username.')

    #When you add any methods that match the pattern validate_<field_name>, WTForms takes those 
    # as custom validators and invokes them in addition to the stock validators
    def validate_email(self, email):
        user = Member.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')
        