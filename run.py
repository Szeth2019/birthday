# import app from init.py
from birthday import app

if __name__ == "__main__":
    app.run()




# python3 -m venv virtual_environment_name
# source virtual_environment_name/bin/activate
# python3 -m venv birthday_venv
# source Birthday/birthday_venv/bin/activate

# set up requirements for venv file: pip freeze > requirements.txt

# pip install -r requirements.txt

# documentation on SQL ALchemy setup: 
#https://flask-sqlalchemy.palletsprojects.com/en/2.x/quickstart/

# log into postgresql
# psql -U postgres
#/Library/PostgreSQL/9.4/data


