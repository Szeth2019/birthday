import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
	SECRET_KEY = "OCML3BRawWEUeaxcuKHLpw"
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SESSION_PERMANENT = False
	SESSION_TYPE = "filesystem"


# class ProductionConfig(Config):
# 	SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")
# 	SQLALCHEMY_TRACK_MODIFICATIONS = False
# 	SECRET_KEY = "OCML3BRawWEUeaxcuKHLpw"
# 	SESSION_PERMANENT = False
# 	SESSION_TYPE = "filesystem"
