from flask import flash, render_template, request, session, url_for, redirect
from birthday import app, db
from datetime import datetime
from birthday.models import Comment, Member, Post
from birthday.forms import LeaveMessage, CreateUser, LoginForm, RegistrationForm
from flask_login import login_user, current_user, logout_user, login_required
from werkzeug.urls import url_parse

@app.route('/', methods=["GET", "POST"])
@app.route('/index', methods=["GET", "POST"])
def index():
	# now = datetime.datetime.now()
	# get today's date
	now = datetime.now()
	birthday = now.month == 9 and now.day == 11

	return render_template("index.html", birthday=birthday)

@app.route('/about')
def about():
	return render_template("about.html")

@app.route('/message', methods=["GET", "POST"])
def message():
	form = LeaveMessage()
	if request.method == "POST":
		if form.validate_on_submit():
			# Add flash functionality later
			# flash(f'You successfully left a message!', 'success')
			name = form.name.data
			comment = form.note.data
			new_comment = Comment(name=name, note=comment)
			db.session.add(new_comment)
			db.session.commit()
			# get all comments from the db
			comments = Comment.query.all()
			return render_template("message.html", comments=comments, form=form)
		else:
			# get all comments from the db
			comments = Comment.query.all()
			return render_template("message.html", comments=comments, form=form)
	else:
		# get all comments from the db
		comments = Comment.query.all()
		return render_template("message.html", comments=comments, form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
	# check if user is logged in, keep away from the login form
	if current_user.is_authenticated:
		return redirect(url_for('message'))

	form = LoginForm()
	if form.validate_on_submit():
    # search user in database sent by form on login.html page
		user = Member.query.filter_by(username=form.username.data).first()
        # if invalid username or password flash invalid username/password 
    	# redirect back tologin page
		if user is None or not user.check_password(form.password.data):
			flash('Invalid username or password')
			return redirect(url_for('login'))
    	# keep user logged in and redirect to home page
		login_user(user, remember=form.remember_me.data)
		return redirect('/index')
	return render_template('login.html', title='Sign In', form=form)

# we need to import logout_user for this
@app.route('/logout', methods=['GET', 'POST'])
def logout():
	logout_user()
	return redirect(url_for('message'))

@app.route('/register', methods=['GET', 'POST'])
def register():
	if current_user.is_authenticated:
		return redirect(url_for('index'))
	form = RegistrationForm()
	if form.validate_on_submit():
		user = Member(username=form.username.data, email=form.email.data)
		user.set_password(form.password.data)
		db.session.add(user)
		db.session.commit()
		flash('Congratulations, you are now a registered user!')
		return redirect(url_for('login'))
	
	return render_template('register.html', title='Register', form=form)
		

@app.route('/wish')
def wish():
	return render_template("wishlist.html")

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500




# @app.route("register", methods=['GET', 'POST'])
# def register
# set form variable to registration form
# check if the user/email is in the database
# if user in database, thrown an error
# if the form is valid, hash the pasword
	# create user in the db, password is hash password
	# db session add and db commit
	# flash to user the account has been created
	# redirect user to home page
# if none of above, render register page



# @app.route('/signup', methods=['GET', 'POST'])
# def signup_page():
#     """User sign-up page."""
#     signup_form = SignupForm(request.form)
#     # POST: Sign user in
#     if request.method == 'POST':
#         if signup_form.validate():
#             # Get Form Fields
#             name = request.form.get('name')
#             email = request.form.get('email')
#             password = request.form.get('password')
#             website = request.form.get('website')
#             existing_user = User.query.filter_by(email=email).first()
#             if existing_user is None:
#                 user = User(name=name,
#                             email=email,
#                             password=generate_password_hash(password, method='sha256'),
#                             website=website)
#                 db.session.add(user)
#                 db.session.commit()
#                 login_user(user)
#                 return redirect(url_for('main_bp.dashboard'))
#             flash('A user already exists with that email address.')
#             return redirect(url_for('auth_bp.signup_page'))
#     # GET: Serve Sign-up page
#     return render_template('/signup.html',
#                            title='Create an Account | Flask-Login Tutorial.',
#                            form=SignupForm(),
#                            template='signup-page',
#                            body="Sign up for a user account.")