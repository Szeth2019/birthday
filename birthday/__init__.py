from flask import Flask
# from datetime import datetime
import os
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_migrate import Migrate

# creating instances of plugin objects
# these are global instances
db = SQLAlchemy()

def create_app():
    # Create flask object and configure it using a class called Config in a file named config.py
    app = Flask(__name__)
    # initialize plugins
    db.init_app(app)
    app.config.from_object('config.Config')

    # create the app context
    with app.app_context():
        # Create tables for our models
        db.create_all()
        return app

app = create_app()
login = LoginManager(app)
# If a user who is not logged in tries to view a protected page, Flask-Login will automatically redirect the user to the login form
login.login_view = 'login'
migrate = Migrate(app, db)

# avoid circular imports by importing at bottom
from birthday import routes
from birthday.models import Comment

