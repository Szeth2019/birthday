from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from birthday import db, login
from flask_login import UserMixin

# class table was used for testing purposes
class Comment(db.Model):
    __tablename__ = "note"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    note = db.Column(db.String, nullable=False)


# Flask-Login keeps track of the logged in user by storing its 
# unique identifier in Flask's user session
# Each time the logged-in user navigates to a new page, 
# Flask-Login retrieves the ID of the user from the session, 
# and then loads that user into memory
@login.user_loader
# takes user id as argument
def load_user(id):
    return Member.query.get(int(id))


class Member(UserMixin, db.Model):
    # this is the site mebers table
    __tablename__ = 'members'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')

    def set_password(self, password):
      self.password_hash = generate_password_hash(password)

    def check_password(self, password):
      return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User2 {}>'.format(self.username)

class Post(db.Model):
    # new post table
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('members.id'))


    def __repr__(self):
        return '<Post {}>'.format(self.body)



